FROM openjdk:18-slim

ENV ENV_BUILD_TOOLS_VERSION="33.0.2"

# Common deps
RUN apt-get update -y
RUN apt-get -y install wget
RUN apt-get -y install unzip

# Nodejs and npm
RUN apt-get -y install curl
RUN curl -fsSL https://deb.nodesource.com/setup_18.x | bash - 
RUN apt-get install -y nodejs
RUN npm -v

# ionic/cli
RUN npm install -g @ionic/cli
 
# Firebase
run npm install -g firebase-tools

# Android sdk
RUN mkdir android-sdk
RUN wget https://dl.google.com/android/repository/commandlinetools-linux-9477386_latest.zip
RUN unzip commandlinetools-linux-9477386_latest -d /android-sdk/
RUN rm -rf commandlinetools-linux-9477386_latest

# setup environment variables
#java
ENV PATH $PATH:$JAVA_HOME/bin/
# android and tools
ENV ANDROID_HOME /android-sdk
ENV ANDROID_SDK_ROOT $ANDROID_HOME
ENV PATH $PATH:$ANDROID_SDK_ROOT/cmdline-tools/bin
ENV PATH $PATH:$ANDROID_SDK_ROOT/platform-tools/bin

# setup sdkmanager
RUN android-sdk/cmdline-tools/bin/sdkmanager --install "build-tools;${ENV_BUILD_TOOLS_VERSION}" --sdk_root=${ANDROID_HOME}
RUN yes | /android-sdk/cmdline-tools/bin/sdkmanager --licenses --sdk_root=${ANDROID_HOME}